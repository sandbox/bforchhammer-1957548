<?php

/**
 * Product key class.
 */
class ProductKey extends Entity {

  protected function defaultLabel() {
    $label = $this->key_sequence;
    if (!empty($this->email)) {
      $label .= ' (' . $this->email . ')';
    }
    return $label;
  }

  protected function defaultUri() {
    return array('path' => 'product-key/' . $this->identifier());
  }
}
