<?php

/**
 * ProductKey Views Controller class.
 */
class ProductKeyViewsController extends EntityDefaultViewsController {

  /**
   * Edit or add extra fields to views_data().
   */
  public function views_data() {
    $data = parent::views_data();

    // Declare "active" field as a boolean field.
    $data['product_keys']['active']['field']['handler'] = 'views_handler_field_boolean';
    $data['product_keys']['active']['filter']['handler'] = 'views_handler_filter_boolean_operator';

    // Link to view product key.
    $data['product_keys']['view_link'] = array(
      'field' => array(
        'title' => t('View link'),
        'help' => t('Provide a simple link to view the product key.'),
        'handler' => 'product_key_handler_field_view_link',
      ),
    );

    // Link to edit product key.
    $data['product_keys']['edit_link'] = array(
      'field' => array(
        'title' => t('Edit link'),
        'help' => t('Provide a simple link to edit the product key.'),
        'handler' => 'product_key_handler_field_view_link_edit',
      ),
    );

    // Link to delete product key.
    $data['product_keys']['delete_link'] = array(
      'field' => array(
        'title' => t('Delete link'),
        'help' => t('Provide a simple link to delete the product key.'),
        'handler' => 'product_key_handler_field_view_link_delete',
      ),
    );

    return $data;
  }
}
