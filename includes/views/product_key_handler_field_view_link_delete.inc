<?php

/**
 * @file
 * Definition of product_key_handler_field_view_link_delete.
 */

/**
 * Field handler to present a delete link.
 *
 * @ingroup views_field_handlers
 */
class product_key_handler_field_view_link_delete extends product_key_handler_field_view_link {

  function render_link($product_key, $values) {
    if (entity_access('delete', 'product_key', $product_key)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "product-key/{$product_key->pkid}/delete";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');
      return $text;
    }
  }
}
