<?php

/**
 * @file
 * Definition of product_key_handler_field_view_link_edit.
 */

/**
 * Field handler to present an edit link.
 *
 * @ingroup views_field_handlers
 */
class product_key_handler_field_view_link_edit extends product_key_handler_field_view_link {

  function render_link($product_key, $values) {
    if (entity_access('update', 'product_key', $product_key)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "product-key/{$product_key->pkid}/edit";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
      return $text;
    }
  }
}
