<?php

class ProductKeyController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;
    $values += array(
      //'product' => 0,
      'key_sequence' => $this->generateKey(),
      'email' => '',
      'note' => '',
      'active' => 1,
      'usage_count' => 0,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );
    return parent::create($values);
  }

  public function save($entity) {
    $entity->changed = REQUEST_TIME;
    return parent::save($entity);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('product_key', $entity);

    $content['key_sequence'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' =>t('Key'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_key',
      '#field_type' => 'text',
      '#entity_type' => 'product_key',
      '#items' => array(array('value' => $entity->key_sequence)),
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($entity->key_sequence))
    );

    $content['email'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' =>t('Email'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_email',
      '#field_type' => 'text',
      '#entity_type' => 'product_key',
      '#items' => array(array('value' => $entity->email)),
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($entity->email))
    );

    $content['note'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' =>t('Note'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_comment',
      '#field_type' => 'text',
      '#entity_type' => 'product_key',
      '#items' => array(array('value' => $entity->note)),
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($entity->note))
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

  /**
   * Creates a new 6-digit key.
   *
   * @todo Make sure we don't loop indefinitely.
   * @todo Make key length configurable.
   */
  private function generateKey() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    do {
      $code = array();
      for ($i = 0; $i < 6; $i++) {
        $n = rand(0, strlen($alphabet)-1);
        $code[$i] = $alphabet[$n];
      }
      $key = implode($code);
    }
    // Loop until we get a "new" key.
    while ($this->check($key, TRUE));
    return $key;
  }

  /**
   * Check whether the given product key exists.
   *
   * @param $key
   *   The key sequence.
   * @param $active_only
   *   Whether to only check active keys. Defaults to TRUE.
   *
   * @return
   *   TRUE if the key exists, FALSE otherwise.
   *
   * @todo add support for filtering by "product"
   */
  public function check($key, $active_only = TRUE) {
    $query = db_select('product_keys', 'pk');
    $query->condition('pk.key_sequence', $key);
    if ($active_only) {
      $query->condition('pk.active', 1);
    }
    return $query->countQuery()->execute()->fetchField();
  }
}
