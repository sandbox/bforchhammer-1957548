<?php
/**
 * @file
 * CRUD functions for product_key entities.
 */

/**
 * Load a product key by id or key sequence.
 */
function product_key_load($id, $reset = FALSE) {
  if (is_numeric($id)) {
    return product_key_load_by_id($id, $reset);
  }
  if (is_string($id)) {
    return product_key_load_by_key($id, $reset);
  }
  // throw Exception
  return NULL;
}

/**
 * Load multiple product keys based on certain conditions.
 */
function product_key_load_multiple($pkids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('product_key', $pkids, $conditions, $reset);
}

/**
 * Load a product key by pkid.
 */
function product_key_load_by_id($pkid, $reset = FALSE) {
  $keys = product_key_load_multiple(array($pkid), array(), $reset);
  return reset($keys);
}

/**
 * Load a product key by key sequence.
 */
function product_key_load_by_key($key, $reset = FALSE) {
  $keys = product_key_load_multiple(FALSE, array('key_sequence' => $key), $reset);
  return reset($keys);
}

/**
 * Save product key.
 */
function product_key_save($product_key) {
  return entity_save('product_key', $product_key);
}

/**
 * Delete single product key.
 */
function product_key_delete($product_key) {
  return entity_delete('product_key', entity_id('product_key' ,$product_key));
}

/**
 * Delete multiple product keys.
 */
function product_key_delete_multiple($product_key_ids) {
  return entity_delete_multiple('product_key', $product_key_ids);
}

/**
 * Checks whether a product key sequence is valid.
 *
 * @param $key
 *   The key sequence.
 * @return
 *   TRUE if the given key is valid, FALSE otherwise.
 */
function product_key_is_valid($key) {
  return entity_get_controller('product_key')->check($key);
}

/**
 * Increases usage count for given key.
 *
 * @param $key
 *   A product_key.pkid, product_key.key_sequence or ProductKey object.
 */
function product_key_usage_count_increase($key) {
  if (!is_object($key)) {
    $key = product_key_load($key);
  }
  if ($key) {
    $key->usage_count++;
    return product_key_save($key);
  }
  else {
    // @todo throw exception?
    return FALSE;
  }
}
