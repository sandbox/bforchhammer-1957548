<?php

/**
 * Product key form.
 */
function product_key_form($form, &$form_state, $product_key = NULL) {
  // Determine product key and store in form_state.
  if (empty($product_key)) {
    $product_key = !empty($form_state['product_key']) ? $form_state['product_key'] : entity_create('product_key', array());
  }
  $form_state['product_key'] = $product_key;
  $form_state['cache'] = TRUE;

  $form['key_sequence'] = array(
    '#type' => 'item',
    '#title' => t('Key'),
    '#markup' => $product_key->key_sequence,
  );

  $form['email'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Email'),
    '#default_value' => $product_key->email,
    '#element_validate' => array('product_key_form_email_validate'),
  );

  $form['note'] = array(
    '#type' => 'textfield',
    '#title' => t('Note'),
    '#default_value' => $product_key->note,
  );

  if (empty($product_key->is_new)) {
    $form['active'] = array(
      '#type' => 'checkbox',
      '#title' => t('Active'),
      '#description' => t('A disabled key cannot be used anymore for ordering products.'),
      '#default_value' => $product_key->active,
    );

    $form['usage_count'] = array(
      '#type' => 'item',
      '#title' => t('Usage count'),
      '#markup' => $product_key->usage_count,
    );
  }

  // @todo product -> node id reference

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('product_key_form_submit'),
  );

  // Show Delete button if we edit product key.
  if (empty($product_key->is_new) && entity_access('delete', 'product_key', $product_key)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('product_key_form_submit_delete'),
    );
  }

  return $form;
}

/**
 * Product key form email validation handler.
 */
function product_key_form_email_validate($element, &$form_state, $form) {
  $email = $element['#value'];
  if (!empty($email)) {
    if (!valid_email_address($email)) {
      form_error($element, t('The email address appears to be invalid.'));
    }
    elseif (!empty($form_state['product_key']->is_new)) {
      $query = db_select('product_keys', 'pk');
      $query->condition('pk.email', $email);
      if ($query->countQuery()->execute()->fetchField()) {
        form_error($element, t('There is already a key associated with %email.', array('%email' => $email)));
      }
    }
  }
}

/**
 * Product key form submit handler.
 */
function product_key_form_submit($form, &$form_state) {
  $product_key = $form_state['product_key'];

  entity_form_submit_build_entity('product_key', $product_key, $form, $form_state);
  product_key_save($product_key);

  $product_key_uri = entity_uri('product_key', $product_key);
  $form_state['redirect'] = $product_key_uri['path'];

  drupal_set_message(t('Product key %key saved.', array('%key' => entity_label('product_key', $product_key))));
}

/**
 * Product key form submit (delete) handler.
 */
function product_key_form_submit_delete($form, &$form_state) {
  $product_key = $form_state['product_key'];
  $product_key_uri = entity_uri('product_key', $product_key);
  $form_state['redirect'] = $product_key_uri['path'] . '/delete';
}

/**
 * Product key add multiple form.
 */
function product_key_multiple_form($form, &$form_state, $product_keys_count = 10) {
  $form_state['cache'] = TRUE;

  $form['product_keys'] = array(
    '#theme' => 'table',
    '#header' => array(
      t('Key'),
      t('Email'),
      t('Note'),
    ),
    '#tree' => TRUE,
    '#prefix' => '<div id="product-keys-wrapper">',
    '#suffix' => '</div>',
    '#process' => array('product_key_process_form_table'),
  );

  for ($i = 0; $i < $product_keys_count; $i++) {
    if (empty($form_state['product_keys'][$i])) {
      $form_state['product_keys'][$i] = entity_create('product_key', array());
    }
    $product_key = $form_state['product_keys'][$i];

    $form['product_keys'][$i]['key_sequence'] = array(
      '#type' => 'item',
      '#title' => t('Key'),
      '#title_display' => 'invisible',
      '#markup' => $product_key->key_sequence,
    );

    $form['product_keys'][$i]['email'] = array(
      '#type' => 'textfield',
      '#title' => t('Email'),
      '#title_display' => 'invisible',
      '#default_value' => $product_key->email,
      '#element_validate' => array('product_key_form_email_validate'),
    );

    $form['product_keys'][$i]['note'] = array(
      '#type' => 'textfield',
      '#title' => t('Note'),
      '#title_display' => 'invisible',
      '#default_value' => $product_key->note,
    );

    // Fields are added directly to the form above, so they'll be named
    // correctly; in order for them to be rendered properly in the table, we
    // also need to add them as table #rows.
    $form['product_keys']['#rows'][] = array(
      array('data' => &$form['product_keys'][$i]['key_sequence']),
      array('data' => &$form['product_keys'][$i]['email']),
      array('data' => &$form['product_keys'][$i]['note']),
    );
  }

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('product_key_multiple_form_submit'),
  );

  $form['actions']['submit_more'] = array(
    '#type' => 'submit',
    '#value' => 'Save and add more',
    '#submit' => array('product_key_multiple_form_submit'),
  );
  return $form;
}

function product_key_process_form_table($element, &$form_state, $form) {
  $values = drupal_array_get_nested_value($form_state['input'], $element['#parents']);
  if (!empty($values)) {
    foreach ($values as $i => $item) {
      foreach ($item as $key => $value) {
        form_set_value($element[$i][$key], $value, $form_state);
        $element[$i][$key]['#value'] = $value;
      }
    }
  }
  return $element;
}

/**
 * Product key add multiple form submit handler.
 */
function product_key_multiple_form_submit($form, &$form_state) {
  $count = 0;
  foreach($form_state['input']['product_keys'] as $i => $value) {
    if (!empty($value['email'])) {
      $product_key = $form_state['product_keys'][$i];
      $product_key->email = $value['email'];
      $product_key->note = $value['note'];
      product_key_save($product_key);
      $count++;
    }
  }
  
  if ($form_state['clicked_button']['#id'] == 'edit-submit-more') {
    $form_state['redirect'] = 'product-key/add-multiple';
  }
  else {
    $form_state['redirect'] = 'product-key';
  }
  drupal_set_message(t('Created %count product keys.', array('%count' => $count)));
}

/**
 * Product key delete confirmation form.
 */
function product_key_delete_form($form, &$form_state, $product_key) {
  $form_state['product_key'] = $product_key;

  // Always provide entity id in the same form key as in the entity edit form.
  $form['product_key_id'] = array('#type' => 'value', '#value' => entity_id('product_key' ,$product_key));

  $product_key_uri = entity_uri('product_key', $product_key);
  return confirm_form($form,
    t('Are you sure you want to delete product key %key?', array('%key' => entity_label('product_key', $product_key))),
    $product_key_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Product key delete confirmation form submit handler.
 */
function product_key_delete_form_submit($form, &$form_state) {
  $product_key = $form_state['product_key'];
  product_key_delete($product_key);

  drupal_set_message(t('Product key %key deleted.', array('%key' => entity_label('product_key', $product_key))));

  $form_state['redirect'] = 'product-key';
}
