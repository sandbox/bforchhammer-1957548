<?php

/**
 * Product key view callback.
 */
function product_key_view($product_key) {
  drupal_set_title(entity_label('product_key', $product_key));
  $pkid = entity_id('product_key', $product_key);
  return entity_view('product_key', array($pkid => $product_key), 'full');
}
